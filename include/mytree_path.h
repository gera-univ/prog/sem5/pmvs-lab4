#pragma once
#include <stdbool.h>
#include <stddef.h>

char **patharray_init(bool is_absolute);
char **patharray_add_token(char **patharray, const char *token);
size_t patharray_size(char **patharray);
void patharray_free(char **patharray);
char **path_to_patharray(const char *path, bool is_absolute);
