#pragma once

#include "mytree_node.h"

mytree_node *mytree_init(void);
mytree_node *mytree_add(mytree_node *parent, char *name);
mytree_node *mytree_find(mytree_node *root, char *p[]);
mytree_node *mytree_find_path(mytree_node *root, const char *path, bool is_absolute);
