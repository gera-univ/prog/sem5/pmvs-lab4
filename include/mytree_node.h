#pragma once

#include <stddef.h>
#include <limits.h>

typedef struct mytree_node {
	char name[NAME_MAX];
	size_t cur_children;
	size_t max_children;
	void *data;
	void *contents;
	struct mytree_node **children;
} mytree_node;

mytree_node *mytree_node_init(char *name);
void mytree_node_free(mytree_node *node);
