#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include "mytree.h"
#include "mytree_node.h"
#include "mytree_path.h"

mytree_node *mytree_init(void)
{	
	return mytree_node_init("/");
}

mytree_node *mytree_add(mytree_node *parent, char *name)
{
	mytree_node *child = mytree_node_init(name);

	if (parent->cur_children == parent->max_children) {
		parent->children = reallocarray(parent->children, parent->max_children, sizeof(mytree_node *));
	}

	++parent->cur_children;
	parent->children[parent->cur_children - 1] = child;

	return child;
}

mytree_node *mytree_find(mytree_node *node, char *p[])
{
	if (strcmp(node->name, p[0]) != 0)
			return NULL;

	size_t path_index = 1;
	while (p[path_index] != NULL) {
		bool found = false;
		for (size_t i = 0; i < node->cur_children; ++i) {
			if (strcmp(node->children[i]->name, p[path_index]) == 0) {
				node = node->children[i];
				++path_index;
				found = true;
				break;
			}
		}
		if (!found)
			return NULL;
	}

	return node;
}

mytree_node *mytree_find_path(mytree_node *root, const char *path, bool is_absolute)
{
	char **patharray = path_to_patharray(path, is_absolute);
	if (patharray == NULL)
		return NULL;
	mytree_node *node = mytree_find(root, patharray);
	free(patharray);
	return node;
}
