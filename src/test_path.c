#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <string.h>
#include "mytree_path.h"

int main(int argc, char **argv)
{
	char **patharray = patharray_init(true);
	
	patharray = patharray_add_token(patharray, "a");
	patharray = patharray_add_token(patharray, "b");
	patharray = patharray_add_token(patharray, "c");

	patharray_free(patharray);

	char path[] = "/usr/local/bin";

	patharray = path_to_patharray(path, true);

	size_t i = 0;
	while (patharray[i] != NULL) {
		printf("%s\n", patharray[i]);
		++i;
	}

	patharray_free(patharray);

	return 0;
}
