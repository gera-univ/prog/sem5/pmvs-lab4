#define FUSE_USE_VERSION 31
#define ECHO_BIN "/bin/echo"
#define ECHO_BIN_SIZE_MAX 1024*1024

#include <fuse.h>
#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <fcntl.h>
#include <stddef.h>
#include <assert.h>
#include <fcntl.h>
#include <unistd.h>
#include "mytree.h"
#include "mytree_path.h"

mytree_node *root_node;

void set_node_data(mytree_node *node, struct stat data)
{
	if (node->data != NULL)
		free(node->data);
	node->data = malloc(sizeof(struct stat));
	memset(node->data, 0, sizeof(struct stat));
	*(struct stat *)node->data = data;
}

void set_node_contents(mytree_node *node, void *contents, size_t n_bytes)
{
	if (node->contents != NULL)
		free(node->contents);
	node->contents = malloc(n_bytes);
	memcpy(node->contents, contents, n_bytes);
}

void fill_tree(void)
{
	root_node = mytree_init();
	set_node_data(root_node, (struct stat) {
		.st_mode = S_IFDIR | 0705,
		.st_nlink = 1
	});

	mytree_node *bar_node = mytree_add(root_node, "bar");
	set_node_data(bar_node, (struct stat) {
		.st_mode = S_IFDIR | 0705,
		.st_nlink = 1
	});

	mytree_node *bin_node = mytree_add(bar_node, "bin");
	set_node_data(bin_node, (struct stat) {
		.st_mode = S_IFDIR | 0700,
		.st_nlink = 1
	});

	mytree_node *baz_node = mytree_add(bar_node, "baz");
	set_node_data(baz_node, (struct stat) {
		.st_mode = S_IFDIR | 0644,
		.st_nlink = 1
	});

	int echo_bin_fd = open(ECHO_BIN, O_RDONLY, S_IRUSR | S_IRGRP | S_IROTH);
	char *buf = malloc(ECHO_BIN_SIZE_MAX);
	int num_read = read(echo_bin_fd, buf, ECHO_BIN_SIZE_MAX);
	close(echo_bin_fd);
	mytree_node *echo_node = mytree_add(bin_node, "echo");
	set_node_data(echo_node, (struct stat) {
		.st_mode = S_IFREG | 0555,
		.st_nlink = 1,
		.st_size = num_read
	});
	set_node_contents(echo_node, buf, num_read);
	free(buf);

	char readme_string[] = "This is readme file\n";
	mytree_node *readme_node = mytree_add(bin_node, "readme.txt");
	set_node_data(readme_node, (struct stat) {
		.st_mode = S_IFREG | 0400,
		.st_nlink = 1,
		.st_size = strlen(readme_string) + 1
	});
	set_node_contents(readme_node, readme_string, strlen(readme_string));

	char example_string[] = "This is example file\n";
	mytree_node *example_node = mytree_add(baz_node, "example");
	set_node_data(example_node, (struct stat) {
		.st_mode = S_IFREG | 0222,
		.st_nlink = 1,
		.st_size = strlen(example_string) + 1
	});
	set_node_contents(example_node, example_string, strlen(example_string));

	mytree_node *foo_node = mytree_add(root_node, "foo");
	set_node_data(foo_node, (struct stat) {
		.st_mode = S_IFDIR | 0233,
		.st_nlink = 1
	});

	char test_string[] = "This is test file\n";
	mytree_node *test_node = mytree_add(foo_node, "test.txt");
	set_node_data(test_node, (struct stat) {
		.st_mode = S_IFREG | 0007,
		.st_nlink = 1,
		.st_size = strlen(test_string) + 1
	});
	set_node_contents(test_node, test_string, strlen(test_string));
}

static void *myfs_init(struct fuse_conn_info *conn,
			struct fuse_config *cfg)
{
	fill_tree();

	cfg->kernel_cache = 1;
	return NULL;
}

static int myfs_readdir(const char *path, void *buf, fuse_fill_dir_t filler,
			 off_t offset, struct fuse_file_info *fi,
			 enum fuse_readdir_flags flags)
{
	mytree_node *node = mytree_find_path(root_node, path, true);
	if (node == NULL) {
		return -ENOENT;
	}

	filler(buf, ".", NULL, 0, 0);
	filler(buf, "..", NULL, 0, 0);
	for (size_t i = 0; i < node->cur_children; ++i) {
		filler(buf, node->children[i]->name, NULL, 0, 0);
	}

	return 0;
}

static int myfs_getattr(const char *path, struct stat *stbuf, struct fuse_file_info *fi)
{
	int res = 0;

	mytree_node *node = mytree_find_path(root_node, path, true);
	memset(stbuf, 0, sizeof(struct stat));
	if (node != NULL && node->data != NULL) {
		*stbuf = *(struct stat *)node->data;
	} else
		res = -ENOENT;

	return res;
}

static int myfs_read(const char *path, char *buf, size_t size, off_t offset,
		      struct fuse_file_info *fi)
{
	mytree_node *node = mytree_find_path(root_node, path, true);
	if (node->contents == NULL)
		return 0;
	size_t len = (*(struct stat *)node->data).st_size;
	if (offset < len) {
		if (offset + size > len)
			size = len - offset;
		memcpy(buf, node->contents + offset, size);
	} else
		size = 0;

	return size;
}

static int myfs_mkdir(const char *path, mode_t mode)
{
	char **patharray = path_to_patharray(path, true);
	size_t i = 0;
	while (patharray[i] != NULL)
		++i;

	char *child_name = patharray[i - 1];
	patharray[i - 1] = NULL; // remove child from path temporarily
	mytree_node *parent = mytree_find(root_node, patharray);
	if (parent == NULL)
		return ENOENT; // parent does not exist
	patharray[i - 1] = child_name; // return child to path

	mytree_node *child = mytree_find(root_node, patharray);
	if (child != NULL)
		return EACCES; // child already exists

	child = mytree_add(parent, child_name);
	set_node_data(child, (struct stat) {
		.st_mode = S_IFDIR | mode,
		.st_nlink = 1
	});

	return 0;
}

static const struct fuse_operations myfs_oper = {
	.init       = myfs_init,
	.getattr    = myfs_getattr,
	.readdir    = myfs_readdir,
	.read       = myfs_read,
	.mkdir      = myfs_mkdir
};

int main( int argc, char *argv[] )
{
	return fuse_main( argc, argv, &myfs_oper, NULL );
}
