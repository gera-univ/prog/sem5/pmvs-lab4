#include <stdlib.h>
#include <string.h>
#include "mytree_node.h"

mytree_node *mytree_node_init(char *name)
{
	mytree_node *node = malloc(sizeof(mytree_node));
	node->cur_children = 0;
	node->max_children = 0;
	node->children = NULL;
	strcpy(node->name, name);
	return node;
}

void mytree_node_free(mytree_node *node)
{
	for (size_t i = 0; i < node->cur_children; ++i) {
		mytree_node_free(node->children[i]);
	}
	if (node->data != NULL) free(node->data);
	if (node->contents != NULL) free(node->contents);
	free(node);
}
