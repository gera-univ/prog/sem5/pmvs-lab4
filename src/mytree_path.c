#include "mytree_path.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>

char **patharray_init(bool is_absolute)
{
	char **patharray = calloc(1, sizeof(char *));
	patharray[0] = NULL;
	if (is_absolute)
		patharray_add_token(patharray, "/");
	return patharray;
}

char **patharray_add_token(char **patharray, const char *token)
{
	size_t new_size = patharray_size(patharray) + 1;
	patharray = reallocarray(patharray, new_size + 1, sizeof(char *));

	patharray[new_size - 2] = calloc(NAME_MAX, sizeof(char));
	strcpy(patharray[new_size - 2], token);

	patharray[new_size - 1] = NULL;
	return patharray;
}

size_t patharray_size(char **patharray)
{
	if (patharray == NULL)
		return 0;
	size_t size = 1;
	while (patharray[size - 1] != NULL)
		++size;
	return size;
}

void patharray_free(char **patharray)
{
	size_t i = 0;
	while (patharray[i] != NULL) {
		free(patharray[i]);
		++i;
	}
	free(patharray);
}

char **path_to_patharray(const char *path, bool is_absolute)
{
	size_t path_length = strlen(path) + 1;
	char *buf = calloc(path_length, sizeof(char));
	memcpy(buf, path, path_length);

	char **patharray = patharray_init(is_absolute);

	char *token = strtok(buf, "/");
	while (token != NULL) {
		patharray = patharray_add_token(patharray, token);
		token = strtok(NULL, "/");
	}
	free(buf);

	return patharray;
}
